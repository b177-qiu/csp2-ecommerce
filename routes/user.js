const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");


// endpoint: /users/checkEmail
router.post("/checkEmail", (req, res)=> {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


router.post("/register", (req, res)=> {
	userController.registerUser(req.body).then(resultFromController=> res.send(resultFromController));
})

router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(resultFromController => res.send (resultFromController));
})

// endpoint: /users
router.get("/", (req,res)=>{
	userController.getAllUsers(req.body).then(resultFromController => res.send (resultFromController));
})


router.get("/details", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);

	userController.getUserDetails({userId:userData.id}).then(resultFromController => res.send (resultFromController));
})

router.put("/:id/setAsAdmin",(req,res)=>{
	userController.setAsAdmin(req.params.id, req.body).then(resultFromController=>res.send(resultFromController));
})

router.post("/checkout", auth.verify, (req, res) => {
	const data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	}
	userController.checkOut(data).then(resultFromController => res.send(resultFromController));
})

router.get("/orders", auth.verify,(req,res)=>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		orders: req.body.orders
	}
	userController.getAllOrders(data).then(resultFromController => res.send (resultFromController));
})

router.get("/myOrders", auth.verify, (req,res)=>{
	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		orders: req.body.orders,
	}
	userController.myOrders(data).then(resultFromController => res.send (resultFromController));
})

module.exports = router;
