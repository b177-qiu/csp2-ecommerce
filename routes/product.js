const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Create a Product /products
router.post("/", auth.verify, (req, res)=>{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController))
})


// GET all active products
router.get("/",(req,res)=>{
	productController.getAllProduct().then(resultFromController=>res.send(resultFromController));
})

router.get("/all",auth.verify,(req,res)=>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.getAllProducts(data).then(resultFromController=>res.send(resultFromController));
})

// GET specific product
router.get("/:productId", (req,res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController))
})

// Update Product
router.put("/:productId", auth.verify, (req, res)=>{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(data, req.params).then(resultFromController => res.send(resultFromController));
})

// Archive Product

router.put("/:productId/archive",auth.verify, (req,res)=>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(data,req.params).then(resultFromController=>res.send(resultFromController));
})


router.put("/:productId/activate",auth.verify, (req,res)=>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.activateProduct(data,req.params).then(resultFromController=>res.send(resultFromController));
})

module.exports = router;
