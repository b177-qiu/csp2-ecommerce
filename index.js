const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");

const app = express();

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.wkugf.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", ()=> console.log("You are now conencted to MongoDB."));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);

app.use("/products", productRoutes);


app.listen(process.env.PORT || 4001, ()=>{
	console.log(`Now online at port ${process.env.PORT || 4001}`)
});

