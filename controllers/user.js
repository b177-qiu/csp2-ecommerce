const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody)=>{
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}
// Registration
module.exports.registerUser = (reqBody)=>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error)=> {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Authentication (/login)
module.exports.loginUser = (reqBody)=>{
	return User.findOne({email:reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return {access : auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}
	})
}

//get all users
module.exports.getAllUsers = () => {
	return User.find({}).then(result=>{
		return result;
	})
}

// get user details
module.exports.getUserDetails = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;

	});

};

module.exports.setAsAdmin = (userId,updateAdmin)=>{
	return User.findByIdAndUpdate(userId).then((result,error)=>{
		if(error){
			console.log(error);
			return false; 
		}

		result.isAdmin = "true";	

		return result.save().then((updatedAdmin, saveErr)=> {
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updatedAdmin;
			}
		})
	})
}

module.exports.checkOut = async(data) => {
	if (data.isAdmin == false){
		let isUserUpdated = await User.findById(data.userId).then(user => {
            user.orders.push({productId : data.productId});
    
            return user.save().then((user, error) => {
                if(error) {
                    return false;
                }
                else{
                    return true;
                }
            })
        })

		let isOrderUpdated = await Product.findById(data.productId).then(product => {
            product.buyers.push({userId : data.userId});

            return product.save().then((product, error) => {
                if(error) {
                    return false;
                }
                else{
                    return true;
                }
            })
        })
		if(isUserUpdated && isOrderUpdated) {
            return true;
        }
        else {
            return false;
        }
    }
    else{
        return Promise.resolve("Not available");
    }
}


module.exports.getAllOrders = (data)=>{
	return User.find({}).then(result => {
		if(data.isAdmin == true){

			return result;
		}
		else{
			return ("Not authorised");
		}

	})
}


module.exports.myOrders = (data) => {
	if (data.isAdmin == false){
		return User.findById(data.userId).then(result => {
			return result;
		})
	}
	else{
        return Promise.resolve("Not available");
    }
}
