const Product = require("../models/Product");
const User = require("../models/User");
const auth = require("../auth");

module.exports.addProduct = (data) => {
	if (data.isAdmin) {

		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	} else {
		return Promise.resolve(false);
	}; 
};

// GET all active products
module.exports.getAllProduct = (reqParams, reqBody) => {
	let activeProducts= {
		isActive: true
	}
	return Product.find(activeProducts).then(result => {
		return result;
	})
}

// GET all products as admin
module.exports.getAllProducts = (data) => {
	if (data.isAdmin) {

	return Product.find({}).then(result => {
		return result;
		})
	}
}


// RETRIEVE Product details
module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result=> {
		return result;
	})
}

// UPDATE a product

module.exports.updateProduct = (data,reqParams)=>{

	if (data.isAdmin) {
		let updatedProduct={
			name: data.product.name,
			description:data.product.description,
			price: data.product.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error)=>{
			if(error){
				return false;
			}
			else{
			return true;
			}
		})

	}
	else {
		return Promise.resolve("Not authorised");
	}; 
}; 

// ARCHIVE PRODUCT

module.exports.archiveProduct = (data,reqParams)=>{

	if (data.isAdmin) {
		let updatedStatus={
		isActive: false
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedStatus).then((product,error)=>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else {
		return Promise.resolve("Not authorised");
	}; 
};


module.exports.activateProduct = (data,reqParams)=>{

	if (data.isAdmin) {
		let updatedStatus={
		isActive: true
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedStatus).then((product,error)=>{
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else {
		return Promise.resolve("Not authorised");
	}; 
};
