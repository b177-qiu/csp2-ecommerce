const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required: [true, "First name is required"]
	},
	lastName : {
		type: String,
		required: [true, "Last name is required"]
	},
	email : {
		type: String,
		required: [true, "Email is required"]
	},
	password : {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin : {
		type: Boolean,
		default: false
	},
	orders : [{
		 productId : {
			 type: String,
			 default: true
		   },
		 totalAmount : {
			 	type: Number,
			 	default: true
		 	},
		 purchasedOn:{
				type: Date,
				default: new Date()
		 	},
		 status:{
				type: String,
				default: "Checked out"
		 	}
	}]
})


module.exports = mongoose.model("User", userSchema);
